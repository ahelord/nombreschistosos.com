import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaNombresChistososComponent } from './lista-nombres-chistosos.component';

describe('ListaNombresChistososComponent', () => {
  let component: ListaNombresChistososComponent;
  let fixture: ComponentFixture<ListaNombresChistososComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaNombresChistososComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaNombresChistososComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
