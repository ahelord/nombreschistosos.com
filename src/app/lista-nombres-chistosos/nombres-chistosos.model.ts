export class NombresChistosos {


  private _nombres: string;

  private _apellidos: string;

  alias: string;
  violo: boolean;
  porQuien: string;


  constructor(nombres: string) {
    this._nombres = nombres;
  }


  /*-----nombres-----*/
  get nombres(): string {
    return this._nombres;
  }

  set nombres(value: string) {
    this._nombres = value;
  }

  /*-----apellidos-----*/
  get apellidos(): string {
    return this._apellidos;
  }

  set apellidos(value: string) {
    this._apellidos = value;
  }



  /*--suma--*/

  miSuma(y:number, x:number):number {
    let suma=x+y
    return suma
  }

  miDivi(a:number, b:number):number {
    return a / b
  }

  obtenerElNombreCompletoAliasVioladoPorQuien():string {
    return this._nombres + ' ' + this._apellidos + '' + this.alias + '' +this.violo + '' +this.porQuien
  }


}

