import {Component, OnInit} from '@angular/core';
import {NombresChistosos} from './nombres-chistosos.model';

@Component({
  selector: 'app-lista-nombres-chistosos',
  templateUrl: './lista-nombres-chistosos.component.html',
  styleUrls: ['./lista-nombres-chistosos.component.css']
})
export class ListaNombresChistososComponent {
  miPrimerNombreChistoso: NombresChistosos;
  miSegundoNombreChistoso: NombresChistosos;
  arregloNombresChistosos: Array<NombresChistosos>;
  nombreChistosoIngresado: NombresChistosos;

  constructor() {

    this.nombreChistosoIngresado = new NombresChistosos('nombre ingresado')
    this.miPrimerNombreChistoso = new NombresChistosos('mi segundo nombre');
    this.miPrimerNombreChistoso.nombres = 'Zamapa';
    this.miPrimerNombreChistoso.apellidos = 'Teste';
    this.miPrimerNombreChistoso.alias = 'loka';
    this.miPrimerNombreChistoso.violo = true;
    this.miPrimerNombreChistoso.porQuien = 'su papa';

    console.log(this.miPrimerNombreChistoso.obtenerElNombreCompletoAliasVioladoPorQuien());

    /*------------------------- 2 -----------------------*/
    this.miSegundoNombreChistoso = new NombresChistosos('mi primer nombre');
    this.miSegundoNombreChistoso.nombres = 'Elba';
    this.miSegundoNombreChistoso.apellidos = 'Lazo';
    this.miSegundoNombreChistoso.alias = 'Tiro fijo';
    this.miSegundoNombreChistoso.violo = true;
    this.miSegundoNombreChistoso.porQuien = 'el tio';


    this.cambiarMensajeSegunValidacionViolo(this.miSegundoNombreChistoso);

    let miTercerNombre = new NombresChistosos('mi nombre');
    console.log(miTercerNombre.nombres)
    console.log(miTercerNombre.nombres = 'mi nombre cambiado')

    let miCuartoNombre = new NombresChistosos('mi apellido');
    console.log(miCuartoNombre.apellidos)
    console.log(miCuartoNombre.apellidos = 'mi apellido cambio')

    miCuartoNombre.miSuma(3, 7);

    miCuartoNombre.miDivi(2, 6);

    this.arregloNombresChistosos = [];
    this.arregloNombresChistosos.push(this.miPrimerNombreChistoso);
    this.arregloNombresChistosos.push(this.miSegundoNombreChistoso);


  }

  resetNombres() {
    this.nombreChistosoIngresado= new NombresChistosos ('');
  }

//esta funcion me permite agregar uno nuevo
  agregarNombre(miNombreChisto: NombresChistosos) {
    this.arregloNombresChistosos.push(miNombreChisto);
    this.resetNombres();
  };

  cambiarMensajeSegunValidacionViolo(miNombreChisto: NombresChistosos) {
    if (miNombreChisto.violo) {
      console.log('si violo');
    }
    else {
      console.log('no violo');
    }
  };


}
